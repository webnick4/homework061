import React, { PureComponent } from 'react';
import './List.css';

class List extends PureComponent {
  render() {
    return (
      <li
        className="List"
        onClick={this.props.clicked}
      >{this.props.name}</li>
    );
  }
}

export default List;
import React, { Component, Fragment } from 'react';
import './CountryBuilder.css';
import List from "../../components/List/List";
import axios from 'axios';
import CountryDetails from "../../components/CountryDetails/CountryDetails";
import './CountryBuilder.css';


class CountryBuilder extends Component {
  state = {
    countries: [],
    selectedCountryCode: null
  };

  countrySelectedHandler = code => {
    this.setState({selectedCountryCode: code});
  };

  componentDidMount() {
    const BASE_URL = 'https://restcountries.eu/rest/v2/';

    axios.get(BASE_URL + 'all?fields=name;alpha3Code').then(response => {
      this.setState({countries: response.data});
    }).catch(error => {
      console.log(error);
    });
  }

  render() {
    let countryDetail = null;
    if (this.state.selectedCountryCode === null) {
      countryDetail = (
        <section className="CountryBuilder-main">
          <p>Choose the country</p>
        </section>
      );
    }

    return (
      <Fragment>
        <aside className="CountryBuilder">
          <ul className="CountryBuilder-list">
            {
              this.state.countries.map(country => (
                <List
                  key={country.alpha3Code}
                  name={country.name}
                  clicked={() => this.countrySelectedHandler(country.alpha3Code)}
                />
              ))
            }
          </ul>
        </aside>

        <section className="CountryBuilder-main">
          <CountryDetails code={this.state.selectedCountryCode} />
        </section>

        {countryDetail}
      </Fragment>
    );
  }
}

export default CountryBuilder;
import React, { Component } from 'react';
import CountryBuilder from "./containers/CountryBuilder/CountryBuilder";
import './App.css';

class App extends Component {
  render() {
    return <CountryBuilder className="App" />;
  }
}

export default App;

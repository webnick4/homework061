import React, { Component } from 'react';
import './CountryDetails.css';
import axios from 'axios';

class CountryDetails extends Component {
  state = {
    borders: [],
    loadedCountry: null
  };

  componentDidUpdate() {
    const BASE_URL = 'https://restcountries.eu/rest/v2/';

    if (this.props.code) {
      if (!this.state.loadedCountry || (this.state.loadedCountry && this.state.loadedCountry.alpha3Code !== this.props.code)) {

        axios.get(BASE_URL + 'alpha/' + this.props.code).then(response => {
          this.setState({loadedCountry: response.data});

          return Promise.all(response.data.borders.map(code => {
            return axios.get(BASE_URL + 'alpha/' + code).then(response => {
              return response.data.name;
            })
          })).then(borders => {
            this.setState({borders});
          }).catch(error => console.log(error));
        });
      }
    }
  }

  render() {
    if (this.state.loadedCountry === null) {
      return null;
    }

    return (
      <div className="CountryDetails">
        <h3 className="CountryDetails-title">{this.state.loadedCountry.name}</h3>
        <p><span className="CountryDetails-accent">Flag:</span><img src={this.state.loadedCountry.flag} className="CountryDetails-img" alt="" /></p>
        <p><span className="CountryDetails-accent">Region:</span>{this.state.loadedCountry.region}</p>
        <p><span className="CountryDetails-accent">Capital:</span>{this.state.loadedCountry.capital}</p>
        <p><span className="CountryDetails-accent">Population:</span>{this.state.loadedCountry.population}</p>
        <p><span className="CountryDetails-accent">Border:</span> {this.state.borders.join(', ')}</p>
      </div>
    );
  }
}

export default CountryDetails;